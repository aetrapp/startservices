## AeTrapp Services

This server runs the following services:

- api.aetrapp.org: Main API
- ips.aetrapp.org: Image Processing Service
- painel.aetrapp.org: Dashboard

### Configuration

NGINX configuration can be opened at the editor using nano:

sudo nano /etc/nginx/sites-enabled/default

PM2 is used manage Node.js processes. The configuration file is named ecosystem.config.js. Please check PM2 documents to learn how to stop/start processes.

### Deploying

1. git pull
2. yarn
3. sequelize db:migrate --url "postgresql://aetrapp_db_user:<password>@<host>/aetrapp"
4. pm2 startOrReload ecosystem.config.js --update-env
