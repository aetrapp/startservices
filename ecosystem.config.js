module.exports = {
  apps : [
    {
      name      : 'api',
      cwd      : '<API server directory>',
      script    : 'src',
      env: {
      	NODE_CONFIG: {
          "webappUrl": "<url Dashboard>",
          "apiUrl": "<url API>",
          "ipsUrl": "<url IPS>",
          "sequelize": {
            "dialect": "postgresql",
            "host": "<host do SGDB>",
            "database": "<aetrapp database>",
            "username": "<db user>",
            "password": "<password db user>",
            "port": "<db port>"
          },
          "supportEmail": "<suport email>",
          "oneSignal": {
            "API_KEY": "<one signal api key>",
            "APP_ID": "<one signal app id>"
          },
          "mailgun": {
            "api_key": "mailgun api key",
            "domain": "aetrapp mailgun domain",
            "senderEmail": "AeTrapp Messenger donotrespond@aetrapp.mail"
          }
        }
      }
    },
    {
      name      : 'ips',
      cwd      : '<IPS server directory>',
      script    : 'server',
      instance_var: 'ips-instance',
      env: {
        NODE_ENV: "production",
        NODE_CONFIG: {
          "mongoConnectionString": "mongodb://localhost/agenda",
	  "imagesPath": "<IPS images directrory>"
        }
      }
    }
  ],

  /**
   * Deployment section
   * http://pm2.keymetrics.io/docs/usage/deployment/
   */
  deploy : {
    production : {
      user : '<server user>',
      host : '<hostname or host IP>',
      ref  : 'origin/master',
      repo : 'https://github.com/aetrapp/aetrapp.git',
      path : '<IPS production directory>',
      'post-deploy' : 'yarn install && pm2 reload ecosystem.config.js --env production'
    }
  }
};
